import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            result : 0
        };
        this.onClickAdd = this.onClickAdd.bind(this);
        this.onClickSubtract = this.onClickSubtract.bind(this);
        this.onClickMultiply = this.onClickMultiply.bind(this);
        this.onClickDivide = this.onClickDivide.bind(this);
    }

    onClickAdd(){
        this.setState({
            result : this.state.result+1
        });
    }

    onClickSubtract(){
        this.setState({
            result : this.state.result-1
        });
    }

    onClickMultiply(){
        this.setState({
            result : this.state.result*2
        });
    }

    onClickDivide(){
        this.setState({
            result : this.state.result/2
        });
    }

    render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <button onClick={this.onClickAdd}>加1</button>
          <button onClick={this.onClickSubtract}>减1</button>
          <button onClick={this.onClickMultiply}>乘以2</button>
          <button onClick={this.onClickDivide}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

